<?php

/**
 * This file is part of the akkurate4search library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Subvitamine <hello@subvitamine.com>
 * @license http://opensource.org/licenses/MIT MIT
 */

declare(strict_types=1);

namespace Akkurate4Search;

use NilPortugues\Api\Problem\ApiProblem;
use NilPortugues\Api\Problem\ApiProblemResponse;
use NilPortugues\Api\Problem\Presenter\JsonPresenter;

/**
 * PHP SDK for Akkurate For Search
 */
class Content extends Client
{

    /**
     * Search contents
     * @param string $environment Search environment
     * @param null $q Search keyword
     * @param null $entity UUID of parent entity
     * @param array $access Array of IDS to filter the documents accessible to certain users
     * @param array $options
     * @param array $filters
     * @return mixed|ApiProblemResponse
     */
    public function search(
        $environment,
        $q = null,
        $entity = null,
        $access = [],
        $filters = [],
        $options = [
            "pagination" => false,
            "from" => 0,
            "limit" => 10
        ]
    )
    {
        if (empty($q) && empty($entity) && empty($filters)) {
            return new ApiProblemResponse(new JsonPresenter(new ApiProblem(
                400,
                'The "q" or "entity" or "filters" parameter is missing',
                'Missing parameter'
            )));
        }

        try {
            $client = new \GuzzleHttp\Client(['base_uri' => $this->getEndpoint()]);

            // set params & url
            $params = [];
            $uri = 'contents/search';

            if (empty($entity) && !empty($q)) {
                $params['q'] = $q;
            }

            if (!empty($filters)) {
                foreach ($filters as $filter => $searchWord) {
                    $params['filters[' . $filter . ']'] = $searchWord;
                }
            }

            if (!empty($params)) {
                $uri .= '?' . http_build_query($params);
            }

            if (!empty($entity)) {
                $uri .= '/' . $entity;
            }

            $response = $client->get($uri, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken(),
                    'Environment' => $environment
                ],
                'json' => [
                    'access' => $access,
                    'options' => $options
                ]
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            return json_decode($exception->getResponse()->getBody()->getContents());
        }
    }

    /**
     * Create new content
     * @param array $body Request body (show README for mores details)
     * @return mixed
     */
    public function post($body)
    {
        try {
            $client = new \GuzzleHttp\Client(['base_uri' => $this->getEndpoint()]);
            $response = $client->post('contents', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken()
                ],
                'json' => $body
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            return json_decode($exception->getResponse()->getBody()->getContents());
        }
    }

    /**
     * Update existing content
     * @param string $id Id of the content to update
     * @param array $body Request body (show README for mores details)
     * @return mixed
     */
    public function put($id, $body)
    {
        try {
            $client = new \GuzzleHttp\Client(['base_uri' => $this->getEndpoint()]);
            $response = $client->put('contents/' . $id, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken()
                ],
                'json' => $body
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            return json_decode($exception->getResponse()->getBody()->getContents());
        }
    }

    /**
     * Delete existing content
     * @param string $id
     * @return mixed
     */
    public function delete($id)
    {
        try {
            $client = new \GuzzleHttp\Client(['base_uri' => $this->getEndpoint()]);
            $response = $client->delete('contents/' . $id, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken()
                ]
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            return json_decode($exception->getResponse()->getBody()->getContents());
        }
    }

    /**
     * Delete all contents
     * @param ContentType $contentType
     * @return mixed
     */
    public function deleteAll($contentType = null)
    {
        try {
            $client = new \GuzzleHttp\Client(['base_uri' => $this->getEndpoint()]);
            $response = $client->delete('contents/delete/all', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken()
                ],
                'json' => [
                    'docType' => $contentType
                ]
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            return json_decode($exception->getResponse()->getBody()->getContents());
        }
    }
}
