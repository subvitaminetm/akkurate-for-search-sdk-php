<?php

/**
 * This file is part of the akkurate4search library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Subvitamine <hello@subvitamine.com>
 * @license http://opensource.org/licenses/MIT MIT
 */

declare(strict_types=1);

namespace Akkurate4Search;

/**
 * PHP SDK for Akkurate For Search
 */
class Client
{

    protected $endpoint = 'https://search.akkurate.io/api/v1/';
    protected $apiKey;
    protected $token;

    /**
     * Client constructor.
     * @param string $apiKey
     * @param string $endpoint
     */
    public function __construct($apiKey, $endpoint = null)
    {
        if (!empty($endpoint)) {
            $this->endpoint = $endpoint;
        }
        $this->apiKey = $apiKey;
        $this->generateToken();
    }

    /**
     * Generate access token
     */
    public function generateToken()
    {
        try {
            $client = new \GuzzleHttp\Client(['base_uri' => $this->endpoint]);
            $response = $client->post('auth/get-token-with-account-id', [
                'form_params' => [
                    'apiKey' => $this->apiKey
                ]
            ]);
            $this->token = json_decode($response->getBody()->getContents())->access_token;
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            return json_decode($exception->getResponse()->getBody()->getContents());
        }
    }

    /**
     * @return mixed
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }
}
