<?php

/**
 * This file is part of the akkurate4search library
 *
 * For the full copyright and license information; please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Subvitamine <hello@subvitamine.com>
 * @license http://opensource.org/licenses/MIT MIT
 */

declare(strict_types=1);

namespace Akkurate4Search;

use MyCLabs\Enum\Enum;

/**
 * Target Link
 */
class TargetLink extends Enum
{
    const BLANK = '_blank';
    const SELF = '_self';
}
