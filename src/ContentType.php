<?php

/**
 * This file is part of the akkurate4search library
 *
 * For the full copyright and license information; please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Subvitamine <hello@subvitamine.com>
 * @license http://opensource.org/licenses/MIT MIT
 */

declare(strict_types=1);

namespace Akkurate4Search;

use MyCLabs\Enum\Enum;

/**
 * Content types
 */
class ContentType extends Enum
{
    const COMPANY = 'COMPANY';
    const CONTACT = 'CONTACT';
    const LEAD = 'LEAD';
    const NOTE = 'NOTE';
    const COMMENT = 'COMMENT';
    const QUOTATION = 'QUOTATION';
    const ORDER = 'ORDER';
    const INVOICE = 'INVOICE';
    const COPROPERTY = 'COPROPERTY';
    const SUPPLIER = 'SUPPLIER';
    const DOCUMENT = 'DOCUMENT';
    const MEDIA = 'MEDIA';
    const MAIL = 'MAIL';
    const ARTICLE = 'ARTICLE';
    const PRODUCT = 'PRODUCT';
    const LINK = 'LINK';
    const BOOKMARK = 'BOOKMARK';
    const ACTIVITY_EVENT = 'ACTIVITY_EVENT';
    const ADMIN_ACCOUNT = 'ADMIN_ACCOUNT';
    const ADMIN_USER = 'ADMIN_USER';
    const BLOG_ARTICLE = 'BLOG_ARTICLE';
    const BOOKMARK_CATEGORY = 'BOOKMARK_CATEGORY';
    const BOOKMARK_ITEM = 'BOOKMARK_ITEM';
    const CONTACT_ADDRESS = 'CONTACT_ADDRESS';
    const CONTACT_PHONE = 'CONTACT_ADDRESS';
    const CONTACT_EMAIL = 'CONTACT_PHONE';
    const CRM_COMPANY = 'CRM_COMPANY';
    const CRM_CONTACT = 'CRM_CONTACT';
    const CRM_LEAD = 'CRM_LEAD';
    const DOCUMENTATION_PAGE = 'DOCUMENTATION_PAGE';
    const DOCUMENTATION_SECTION = 'DOCUMENTATION_SECTION';
    const EXCHANGE_COMMENT = 'EXCHANGE_COMMENT';
    const FAQ_QUESTION = 'FAQ_QUESTION';
    const GLOSSARY_TERM = 'GLOSSARY_TERM';
    const HELPDESK_CATEGORY = 'HELPDESK_CATEGORY';
    const HELPDESK_REPLY = 'HELPDESK_REPLY';
    const HELPDESK_TAG = 'HELPDESK_TAG';
    const HELPDESK_TICKET = 'HELPDESK_TICKET';
    const MEDIA_RESOURCE = 'MEDIA_RESOURCE';
    const OTHER = 'OTHER';
    const ENTITY = 'ENTITY';
    const CONTRACT = 'CONTRACT';
}
