# PHP SDK pour l'api d'Akkurate For Search

[![Source Code][badge-source]][source]

## Sommaire

* Installation
* Utilisation
    * Créer du contenu de suggestion
    * Créer du contenu cliquable
    * Mettre à jour un contenu
    * Supprimer un contenu
* Copyright

## Installation

Pour installer la librairie, utilisez [Composer][]. Ajoutez ces instructions à votre fichier `composer.json` :

```bash
composer require subvitaminetm/akkurate-for-search-sdk-php
```

## Utilisation

Initialisez la librairie avec votre clé d'API

```bash
$akk4search = new \Akkurate4Search\Content('your_api_key');
```

#### Créer du contenu de suggestion

Un contenu de suggestion est un contenu parent. Celui-ci remontera dans les résultats de recherche mais n'ouvrira aucun document. Il apparaitra sous forme de suggestion. Si l'utilisateur clique dessus, les documents enfants (cliquables) de ce contenu seront affichés.

Retoune le contenu créé

```bash
$content = $akk4search->post([
    "name" => "Subvitamine",
    "docType" => \Akkurate4Search\ContentType::COMPANY,
    "alias" => [
        "ALIAS_1",
        "ALIAS_2",
        "ALIAS_3"
    ],
    "suggest" => true,
    "entities" => [],
    "content" => [],
    "links" => [],
    "access" => [
        'UUID_OF_USER_1',
        'UUID_OF_USER_2'
    ],
    "environments" => [
        \Akkurate4Search\Environment::BACK,
        \Akkurate4Search\Environment::FRONT
    ]
]);
```

#### Créer du contenu cliquable

Un contenu cliquable est un contenu enfant. Celui-ci remontera dans les résultats de recherche et ouvrira le lien que vous aurez défini lorsque l'utilisateur cliquera dessus.

Retourne le contenu créé

```bash
$content = $akk4search->post([
    "name" => "Invoice April 2020",
    "docType" => \Akkurate4Search\ContentType::INVOICE,
    "alias" => [
        "ALIAS#1",
        "ALIAS#2"
    ],
    "suggest" => false,
    "entities" => [
        [
            "uuid" => "uuid_of_akk4search_parent_suggestable_content",
            "name" => "name_of_akk4search_parent_suggestable_content"
        ]
    ],
    "content" => [
        "string_of_content_1",
        "string_of_content_2",
        "string_of_content_3"
    ],
    "links" => [
        [
            "url" => "https://brain.subvitamine.com/{var1}/{var2}/{var3}",
            "target" => \Akkurate4Search\TargetLink::SELF,
            "environment" => \Akkurate4Search\Environment::BACK
        ],
        [
            "url" => "https://www.subvitamine.com/blog/{var1}",
            "target" => \Akkurate4Search\TargetLink::BLANK,
            "environment" => \Akkurate4Search\Environment::FRONT
        ]
    ],
    "access" => [
        'UUID_OF_USER_1',
        'UUID_OF_USER_2'
    ],
    "environments" => [
        \Akkurate4Search\Environment::FRONT,
        \Akkurate4Search\Environment::BACK
    ]
]);
```

#### Paramètres de contenu

| Nom          	| Description                                                                                      	| Type               	| Requis 	|
|--------------	|--------------------------------------------------------------------------------------------------	|--------------------	|--------	|
| name         	| Nom du document                                                                                  	| string             	| Oui    	|
| docType      	| Type de contenu                                                                                  	| ContentType        	| Oui    	|
| alias      	| Tableau de tags permettant de faire remonter des documents (ex : initiales, catégories...)        | array(string)        	| Non    	|
| suggest      	| Permet de définir si le contenu apparait dans les suggestions ou dans les résultats de recherche 	| boolean            	| Oui    	|
| entities     	| Permet de rattacher un contenu à un document parent                                              	| array              	| Non    	|
| content      	| Contenu du document au format texte                                                              	| array              	| Non    	|
| links        	| Liens qui seront ouverts lorsque l'utilisateur cliquera sur le résultat                          	| array              	| Non    	|
| access        | Liste des UUID ayant accès à la ressource                                                         | array              	| Non    	|
| environments 	| Permet de définir les environnements d'affichage                                                 	| array(Environment) 	| Non    	|

#### Mettre à jour un contenu

Permet de mettre à jour un contenu

Retourne le contenu mis à jour

```bash
$content = $akk4search->put('uuid_of_akk4search_content', $content);
```

#### Rechercher

Permet de rechercher du contenu

**Pour rechercher tout type de contenu "suggestable ou non"**

```bash
// recherche classique
$content = $akk4search->search(
    $environment, 
    $searchword
);

// recherche en ajoutant des restrictions d'accès
$content = $akk4search->search(
    $environment, 
    $searchword, 
    null, 
    [
        $userUUID1,
        $userUUID2
    ],
    [
        "pagination" => true, // defini si on souhaites utiliser la pagination
        "from" => 0, // équivalent du OFFSET de mysql
        "limit" => 10 // équivalent du LIMIT de mysql
    ]
);
```

**Pour rechercher des contenus "non-suggestables"**

```bash
// recherche classique
$content = $akk4search->search(
    $environment, 
    null, 
    $entityUUID
);

// recherche en ajoutant des restrictions d'accès
$content = $akk4search->search(
    $environment, 
    null, 
    $entityUUID, 
    [
        $userUUID1,
        $userUUID2
    ]
);

// recherche en ajoutant des restrictions d'accès et pagination
$content = $akk4search->search(
    $environment, 
    null, 
    $entityUUID, 
    [
        $userUUID1,
        $userUUID2
    ],
    [
        "pagination" => true,
        "from" => 5,
        "limit" => 2
    ]
);
```

Retourne un array contenant trois propriétés.

```bash
[
    'suggestions' => [], // tableau contenant la liste des contenus "suggestables" trouvés
    'results' => [], // tableau contenant la liste des contenus "non-suggestables" trouvés
    'options' => [ // si la pagination est utilisée, réutilisez le tableau d'options retourné pour obtenir les résultats de la page suivante
        'pagination' => true | false
        'from" => 0 | ??,
        'limit' => 10 | ??
    ]
]
```

**Important**

Si vous utilisez [VueJs component for Akkurate for search][], vous devrez utiliser cette fonctionnalité de recherche et il faudra **impérativement** retourner les résultats tel quel mais au format JSON. Voici un exemple :

```bash
$results = $akk4search->search($environment, $searchword);
ou
$results = $akk4search->search($environment, null, $entityUUID);

// return 
echo json_encode($results);
```

ou encore

```bash

echo json_encode($results);
```

#### Supprimer un contenu

Permet de supprimer un contenu

Retourne true(boolean) en cas de succès

```bash
$akk4search->delete('uuid_of_akk4search_content');
```

#### Supprimer tous vos contenus

Permet de supprimer tous vos contenus en une fois

Retourne true(boolean) en cas de succès

```bash
// supprimer tous vos contenus
$akk4search->deleteAll();

or

// supprimer tous vos contenus de type "DOCUMENT"
$akk4search->deleteAll(Akkurate4Search\ContentType::DOCUMENT);
```

## Copyright

Cette librairie est protégée par le droit d'auteur © [Subvitamine](https://www.subvitamine.com)

[composer]: http://getcomposer.org/
[badge-source]: http://img.shields.io/badge/source-akkurate4search-blue.svg?style=flat-square
[source]: https://bitbucket.org/subvitaminetm/akkurate-for-search-sdk-php/src
[VueJs component for Akkurate for search]: https://www.npmjs.com/package/akk4search_vuejs
