<?php

declare(strict_types=1);

namespace Akkurate4Search\Test;

use Akkurate4Search\Client;

class ClientTest extends TestCase
{
    public function testGetHello()
    {
        $object = \Mockery::mock(Client::class);
        $object->shouldReceive('getHello')->passthru();

        $this->assertSame('Hello, World!', $object->getHello());
    }
}
